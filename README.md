# Credit Card Fraud

Load in and explore payment transaction data
Train a LinearLearner to classify the data
Improve a basic model by accounting for class imbalance in the dataset and different metrics for model "success"